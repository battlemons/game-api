package com.example.battlemonsgameapi.controller;

import com.example.battlemonsgameapi.models.Pokemon;
import com.example.battlemonsgameapi.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PokemonController {

    @Autowired
    PokemonRepository pokemonRepository;

    // Get all Pokemon
    @GetMapping("/pokemon")
    public List<Pokemon> getAllPokemon() {
        return pokemonRepository.findAll();
    }

    @GetMapping("/pokemon/{id}")
    public Pokemon getPokemonById(@PathVariable("id") String id) {

        Optional<Pokemon> result = pokemonRepository.findById(Long.parseLong(id));

        try {
            if (result.isPresent()) {
                return result.get();
            }
            else {
                throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Pokemon not found");
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Provide correct pokemon Id", ex);
        }
    }
}

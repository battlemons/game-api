package com.example.battlemonsgameapi.controller;


import com.example.battlemonsgameapi.models.Move;
import com.example.battlemonsgameapi.repository.MoveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class    MoveController {

    @Autowired
    MoveRepository moveRepository;

    // Get all Pokemon
    @GetMapping("/moves")
    public List<Move> getAllMoves(){
        return moveRepository.findAll();
    }
}

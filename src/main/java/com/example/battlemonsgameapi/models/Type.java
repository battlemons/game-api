package com.example.battlemonsgameapi.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pokemon_type")
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Type implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    private String description;
}

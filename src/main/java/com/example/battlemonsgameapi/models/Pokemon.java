package com.example.battlemonsgameapi.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.util.Collection;


@Entity
@Table(name = "pokemon")
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Pokemon implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name="type_id")
    private Type type;

    private int hp;

    private int speed;


    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="pokemon_moves_relation",joinColumns=@JoinColumn(name="pokemonid"),inverseJoinColumns=@JoinColumn(name="moveid") )
    private Collection<Move> moves;

}

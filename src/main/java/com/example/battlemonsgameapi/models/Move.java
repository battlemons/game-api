package com.example.battlemonsgameapi.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pokemon_moves")
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class Move implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String description;

    @Column(name = "min_dmg")
    private int minDamage;

    @Column(name = "max_dmg")
    private int maxDamage;

    @ManyToOne
    @JoinColumn(name="type_id")
    private Type type;

}

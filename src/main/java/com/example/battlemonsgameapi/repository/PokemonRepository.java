package com.example.battlemonsgameapi.repository;


import com.example.battlemonsgameapi.models.Pokemon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PokemonRepository extends JpaRepository <Pokemon, Long> {
}

package com.example.battlemonsgameapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BattlemonsgameapiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BattlemonsgameapiTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testRetrieveAllPokemon() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/api/pokemon", HttpMethod.GET, entity, String.class);
        int expected = 200;

        assertEquals(expected, response.getStatusCodeValue());
    }

    @Test
    public void testRetrievePokemon() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/api/pokemon/5", HttpMethod.GET, entity, String.class);
        int expected = 200;

        assertEquals(expected, response.getStatusCodeValue());
    }

    @Test
    public void testRetrieveUnknownPokemon() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/api/pokemon/999999999999999", HttpMethod.GET, entity, String.class);
        int expected = 400;

        assertEquals(expected, response.getStatusCodeValue());
    }

    @Test
    public void testRetrievePokemonWithString() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + port + "/api/pokemon/test", HttpMethod.GET, entity, String.class);
        int expected = 500;

        assertEquals(expected, response.getStatusCodeValue());
    }
}
